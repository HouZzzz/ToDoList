package todo.Entities;

public class User {
    private int id;
    private String name;
    private String password;

// конструкторы
    public User(int id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }
    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }
    public User() {
    }

// геттеры
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getPassword() {
        return password;
    }

// сеттеры
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
