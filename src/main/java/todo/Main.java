package todo;

import todo.Logic.UserLogic;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Войти/Зарегистрироваться?\n[в/з]");
        String start = scanner.next();

        if (start.equals("в")) {
            System.out.print("Введите логин: ");
            String login = scanner.next();

            System.out.print("Введите пароль: ");
            String password = scanner.next();

            UserLogic.login(login,password);
        } else if (start.equals("з")) {
            System.out.print("Введите логин: ");
            String login = scanner.next();

            System.out.print("Введите пароль: ");
            String password = scanner.next();

            System.out.print("Подтвердите пароль: ");
            String passwordVerify = scanner.next();

            if(password.equals(passwordVerify)) {
                UserLogic.registration(login,password);
            } else {
                System.out.println("Пароли не совпадают");
            }
        }


    }
}