package todo;

import todo.Entities.User;
import todo.Logic.UserLogic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DataContainer {
    private static ResultSet users;
    private static User currnetUser;
    private static ResultSet currentUserDeals;

    public static void updateUsers(String login) {
        DBconnect worker = new DBconnect();

        try {
            Statement statement = worker.getConnection().createStatement();

            // users
            users = statement.executeQuery("SELECT * FROM users");

            // current user
            ResultSet cuQuery = statement.executeQuery("SELECT * FROM users WHERE name = '" + login + "'");
            while (cuQuery.next()) {
                currnetUser = new User(cuQuery.getInt("id"),cuQuery.getString("name"),cuQuery.getString("password"));
            }

            // current deals
            currentUserDeals = statement.executeQuery("SELECT text FROM deals WHERE owner_id = " + currnetUser.getId());


        } catch (SQLException e){
        }
    }

    public static void showUserDeals() {
        Scanner scanner = new Scanner(System.in);
        try {
            if (currnetUser == null) {
                System.out.println("У вас нету дел");
            } else {
                int i = 1;
                while (currentUserDeals.next()) {
                    System.out.println(i + " -> " + currentUserDeals.getString("text") + "\n");
                    i++;
                }
            }

            System.out.println("\n Добавить/удалить дело?\n [д/у]");

            String dealMenu = scanner.next();

            if (dealMenu.equals("д")) {
                UserLogic.addDeal();
            } else if (dealMenu.equals("у")){
                UserLogic.deleteDeal();
            }

        } catch (SQLException e){
        }
    }

    public static void updateUserDeals (int owner_id) {
        DBconnect worker = new DBconnect();

        try {
            Statement statement = worker.getConnection().createStatement();
            currentUserDeals = statement.executeQuery("SELECT * FROM deals WHERE owner_id = " + owner_id);
        } catch (SQLException e) {

        }
    }

    // getters
    public static ResultSet getUsers() {
        return users;
    }

    public static User getCurrnetUser() {
        return currnetUser;
    }

    public static ResultSet getCurrentUserDeals() {
        return currentUserDeals;
    }
}
