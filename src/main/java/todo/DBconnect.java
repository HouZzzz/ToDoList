package todo;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconnect {
    private final String URL = "jdbc:mysql://127.0.0.1:3306/todolist";
    private final String USERNAME = "root";
    private final String PASSWORD = "root";

    private Connection connection;

    public DBconnect() {
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            System.out.println("Ошибка при подклбчении к БД");
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
        } catch (SQLException e) {
            System.out.println("Ошибка при подклбчении к БД");
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
