package todo.Logic;

import todo.DBconnect;
import todo.DataContainer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UserLogic {
    public static void login(String login,String password) {
        DBconnect worker = new DBconnect();

        DataContainer.updateUsers(login);
        System.out.println("\nЗдравствуйте, " + login + "\n");
        DataContainer.showUserDeals();
    }

    public static void registration(String login,String password) {
        DBconnect worker = new DBconnect();

       try {
           Statement statement = worker.getConnection().createStatement();

           if (!statement.execute("INSERT INTO users (name,password)" +
                                         "VALUES ('" + login + "', '" + password + "')" )) {
               DataContainer.updateUsers(login);
               System.out.println("Аккаунт создан");
           } else {
               System.out.println("Аккаунт уже существует");
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }

       System.out.println("\nЗдравствуйте, " + login + "\n");
       DataContainer.showUserDeals();
    }

    public static void addDeal() {
        DBconnect worker = new DBconnect();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите текст дела: ");
        String dealText = scanner.nextLine();

        try {
            Statement statement = worker.getConnection().createStatement();

            statement.execute("INSERT INTO deals (text,owner_id)" +
                    "VALUES ('" + dealText + "', '" + DataContainer.getCurrnetUser().getId() + "')");

            DataContainer.updateUserDeals(DataContainer.getCurrnetUser().getId());
            DataContainer.showUserDeals();
        } catch (SQLException e) {

        }
        DataContainer.showUserDeals();
    }

    public static void deleteDeal() {
        Scanner scanner = new Scanner(System.in);
        DBconnect worker = new DBconnect();

        try {
            Statement statement = worker.getConnection().createStatement();

            List<String> deals = new ArrayList<>();

            ResultSet dealsQuery = statement.executeQuery("SELECT text FROM deals WHERE owner_id = " + DataContainer.getCurrnetUser().getId());

            while (dealsQuery.next()) {
                deals.add(dealsQuery.getString("text"));
            }

            System.out.print("Какое дело вы хотите удалить? (номер) ");
            int dealID = scanner.nextInt();

            statement.execute("DELETE FROM deals WHERE owner_id = " + DataContainer.getCurrnetUser().getId() + " AND text = '" + deals.get(dealID - 1) + "'");

            DataContainer.updateUserDeals(DataContainer.getCurrnetUser().getId());
            DataContainer.showUserDeals();
        } catch (SQLException e) {

        }
    }
}
